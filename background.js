"use strict";

browser.runtime.onMessage.addListener(msg => {
    switch (msg) {
        case 'ready': return browser.bookmarks.getTree();
        default: return false;
    }
});
