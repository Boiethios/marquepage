"use strict";

const main = document.getElementsByTagName('main').item(0);

run().catch(displayError);

async function run() {
    // Create the document structure:
    const rootNodes = await browser.runtime.sendMessage('ready');
    for (const node of rootNodes.flatMap(node => node.children)) {
        TreeItem.factory(node).maybeAppendTo(main, 'section');
    }

    // Add a background style:
    const styleSheet = document.styleSheets[0];
    for (const [i, section] of document.querySelectorAll('section').entries()) {
        section.id = 'section-' + i;
        styleSheet.insertRule(`#${section.id} { background-color: ${stringToColor(section.querySelector('header').textContent)} }`);
    }
}

/**
 * An item in the tree of bookmarks links. It may be empty, or not.
 */
class TreeItem {
    constructor(empty, element) {
        this.element = element;
        this.empty = empty;
    }
    /**
     * Creates an item to be inserted into the links tree.
     * @param { BookmarkTreeNode } A bookmark node.
     * @returns { TreeItem }
     */
    static factory({ type, children, title, url }) {
        switch (type) {
            case 'folder': return TreeItem.folder(title, children);
            case 'bookmark': return TreeItem.bookmark(title, url);
            default: return TreeItem.separator();
        }
    }

    static separator() {
        const item = new TreeItem(true, document.createElement('hr'));
        log('Created separator:', item);
        return item;
    }

    static bookmark(title, url) {
        const link = document.createElement('a');
        link.href = url;
        link.textContent = title;

        const item = new TreeItem(false, link);
        log('Created bookmark:', item);
        return item;
    }

    static folder(title, children) {
        let empty = true;
        const container = document.createElement('div');
        const header = document.createElement('header', title);
        header.textContent = title;
        const list = document.createElement('ul');

        for (const node of children) {
            const item = TreeItem.factory(node);
            empty &&= item.empty;
            item.maybeAppendTo(list, 'li');
        }

        container.appendChild(header);
        container.appendChild(list);

        let item = new TreeItem(empty, empty ? null : container);
        log('Created folder:', item);
        return item;
    }

    /**
     * Appends the item to the container element if the item exists.
     * @param {Element} container The element to which the item will be added.
     * @param {string} encapsulatingTag The tag to create the encapsulating element.
     */
    maybeAppendTo(container, encapsulatingTag) {
        if (this.element != null) {
            const item = document.createElement(encapsulatingTag);

            item.appendChild(this.element);
            container.appendChild(item);
        }
    }
}

/* Error display */

function displayError(error) {
    const addDiv = text => {
        const element = document.createElement('div');
        element.textContent = text;
        main.appendChild(element);
    };

    main.classList.add('error');
    addDiv('An error occured, please file a bug report in the support page.');
    addDiv(error);
    console.log("Error", error)
}

/* Helpers */

function log(...params) {
    //console.log(...params);
}

function stringToColor(str) {
    function cyrb53(str, seed = 0) {
        let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
        for (let i = 0, ch; i < str.length; i++) {
            ch = str.charCodeAt(i);
            h1 = Math.imul(h1 ^ ch, 2654435761);
            h2 = Math.imul(h2 ^ ch, 1597334677);
        }
        h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^ Math.imul(h2 ^ (h2 >>> 13), 3266489909);
        h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^ Math.imul(h1 ^ (h1 >>> 13), 3266489909);
        return 4294967296 * (2097151 & h2) + (h1 >>> 0);
    };

    return '#' + (cyrb53(str) & 0xffffff).toString(16) + "55";
}
